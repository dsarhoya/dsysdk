<?php

namespace DSYSDK\Services\Logs;

use DSYSDK\Services\DSYBaseService as BaseService;

/**
 * Description of LogsClient
 *
 * @author matias
 */
class LogsService extends BaseService{
    
    CONST MASK = '/logs';
    
    public static function newService(\DSYSDK\Clients\DSYBaseClient $client) {
        $service = new LogsService($client);
        return $service;
    }
    
    /**
     * commandLogs: fetch command logs from the server.
     *
     * @return array command logs.
     */
    public function commandLogs($options = array()){
        
        $parameters = array();
        
        if(isset($options['name'])){
            $parameters['name'] = $options['name'];
        }
        
        if(isset($options['date'])){
            
            $date = $options['date'];    
            if(!is_a($date, 'DateTime')) throw new \Exception('date must be DateTime');
            $parameters['date'] = $date->format('Y-m-d');
            
        }elseif (isset($options['date_from']) && isset($options['date_to'])) {
            
            $date_from = $options['date_from'];    
            $date_to = $options['date_to'];    
            
            if(!is_a($date_from, 'DateTime')) throw new \Exception('date_from must be DateTime');
            if(!is_a($date_to, 'DateTime')) throw new \Exception('date_to must be DateTime');
            
            $parameters['date_from'] = $date_from->format('Y-m-d');
            $parameters['date_to'] = $date_to->format('Y-m-d');
            
        }
        
        return $this->client->fetch($this->client->getServiceUrl(self::MASK) . '/commandLogs', $parameters);
    }
    
    public function commandConfigurations($options = array()){
        
        $parameters = array();
        
        if(isset($options['name'])){
            $parameters['name'] = $options['name'];
        }
        
        return $this->client->fetch($this->client->getServiceUrl(self::MASK) . '/configurations', $parameters);
    }
}