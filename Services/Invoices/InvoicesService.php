<?php

namespace DSYSDK\Services\Invoices;

use DSYSDK\Services\DSYBaseService as BaseService;

/**
 * Description of LogsClient
 *
 * @author matias
 */
class InvoicesService extends BaseService{
    
    CONST MASK = '/invoices';
    
    public static function newService(\DSYSDK\Clients\DSYBaseClient $client) {
        $service = new InvoicesService($client);
        return $service;
    }
    
    /**
     * invoices: fetch invoices from the server.
     *
     * @return array invoices.
     */
    public function invoices($options = array()){
        
        $parameters = array();
        
        if(isset($options['date'])){
            
            $date = $options['date'];    
            if(!is_a($date, 'DateTime')) throw new \Exception('date must be DateTime');
            $parameters['date'] = $date->format('Y-m-d');
            
        }
        
        return $this->client->fetch($this->client->getServiceUrl(self::MASK) . '/all', $parameters);
    }
}