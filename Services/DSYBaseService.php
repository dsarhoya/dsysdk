<?php

namespace DSYSDK\Services;

use DSYSDK\Interfaces\DSYServiceInterface;
use DSYSDK\Clients\DSYBaseClient;

/**
 * Description of DSYBaseService
 *
 * @author matias
 */
abstract class DSYBaseService implements DSYServiceInterface {
    
    protected $client;
    
    public function setClient(DSYBaseClient $client){
        $this->client = $client;
    }
    
    public function __construct(DSYBaseClient $client = null) {
        $this->client = $client;
    }
    
}
