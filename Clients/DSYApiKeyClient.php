<?php

namespace DSYSDK\Clients;

use DSYSDK\Clients\DSYBaseClient;
use DSYCredentials\ApiKeyCredentialsGenerator;
use DSYCredentials\ApiKeyConstants;
use GuzzleHttp\Client;

/**
 * Description of DSYApiKeyClient
 *
 * @author matias
 */
class DSYApiKeyClient extends DSYBaseClient
{
    public function fetch($uri, $parameters = null) {

        $parts = explode($this->base_url, $uri);
        
        $parameters_to_sign_array = array();
        $url_to_sign = $parts[1];
        if($parameters){
            $url_to_sign .= '?';
            foreach ($parameters as $parameter => $value) {
                $parameters_to_sign_array[] = sprintf('%s=%s', (string)$parameter, (string)$value);
            }
            $url_to_sign .= implode('&', $parameters_to_sign_array);
        }
        
        $ts = time();
        
        $credentials = new ApiKeyCredentialsGenerator($url_to_sign, $this->client_id, $this->client_secret);
        
        $parameters = array_merge(
                $parameters ? $parameters : array(),
                array(
                    ApiKeyConstants::CREDENTIALS_KEY_TIMESTAMP   =>$credentials->getTimestamp(),
                    ApiKeyConstants::CREDENTIALS_KEY_ID          =>$credentials->getKey(),
                    ApiKeyConstants::CREDENTIALS_KEY_SIGNATURE   =>$credentials->getSignature()
                )
            );

        $client = new Client();
        try{
            $response = $client->get($uri, array('query'=>$parameters));
        }catch(\Exception $e){
            $this->error_type = "Error";
            $this->error_description = $e->getMessage();
            return null;
        }
        
        return $response->json();
    }
}