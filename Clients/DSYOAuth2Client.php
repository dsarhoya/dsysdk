<?php

namespace DSYSDK\Clients;

use DSYSDK\Clients\DSYBaseClient;
use OAuth2\Client;

/**
 * Description of DSYOAuth2Client
 *
 * @author matias
 */
class DSYOAuth2Client extends DSYBaseClient
{   
    CONST TOKEN_OPTION = 'token_option';
    CONST CLIENT_GRANT_TYPE = 'client_credentials';
    
    protected $token_endpoint;
    protected $grant_type;
    protected $OAuth2Client;
    protected $access_token;
    protected $refresh_token;
    protected $access_token_expiration;
    
    /**
     * __construct: client constructor. 
     *
     * @param string $client_id OAuth2 server client id
     * @param string $client_secret OAuth2 server client secret
     * @param string $base_url server base url
     * @param string $mask service client implementation mask
     * @param string $firewall the firewall on the OAuth2 server (optional).
     * @param string $token_endpoint the token end point on the OAuth2 server (optional).
     * @param string $grant_type the client grant type (optional). Needs implementation if different from client credentials
     * @return a DSY Client.
     */
    public function __construct($client_id, $client_secret, $base_url, $firewall = '/api', $token_endpoint = '/oauth/v2/token', $grant_type = self::CLIENT_GRANT_TYPE) {
        
        parent::__construct($client_id, $client_secret, $base_url, $firewall);
        
        $this->token_endpoint       = $token_endpoint;
        $this->grant_type           = $grant_type;
        $this->OAuth2Client         = new Client($this->client_id, $this->client_secret, Client::AUTH_TYPE_AUTHORIZATION_BASIC);
    }
    
    /**
     * loadAccessToken: load a token into the client. Required to access the api.
     *
     * @param array $options pass the access token (optional).
     * @param bool $force  force the client to re negotiate the token. Only useful if a token already exists (optional).
     */
    public function loadAccessToken($options = array(), $force = false){
        
        if($this->access_token && !$force){
            
            return true;
        }
        
        if(isset($options[self::TOKEN_OPTION])){
            $this->access_token = $options[self::TOKEN_OPTION];
            $this->OAuth2Client->setAccessToken($options[self::TOKEN_OPTION]);
            return true;
        }
        
        $response = $this->OAuth2Client->getAccessToken($this->base_url . $this->token_endpoint, $this->grant_type, array());
        
        if(isset($response['result']['error'])){
            $this->error_type           = $response['result']['error'];
            $this->error_description    = $response['result']['error_description'];
            return false;
        }
        
        $this->access_token             = $response['result']['access_token'];
        $this->access_token_expiration  = $response['result']['expires_in'];
        $this->refresh_token            = isset($response['result']['refresh_token']) ? $response['result']['refresh_token'] : '';
        
        $this->OAuth2Client->setAccessToken($this->access_token);
        
        return true;
    }
    
    public function getAccessToken(){
        return $this->access_token;
    }
    
    public function fetch($uri, $parameters = null){
        $response = $this->OAuth2Client->fetch($uri, $parameters);
        return $response['result'];
    }
}