<?php

namespace DSYSDK\Clients;

use DSYSDK\Interfaces\DSYClientInterface;

use DSYSDK\Services\Invoices\InvoicesService;
use DSYSDK\Services\Logs\LogsService;

abstract class DSYBaseClient implements DSYClientInterface
{
    
    CONST CLIENT_SERVICE_TYPE_LOGS = 'logs';
    CONST CLIENT_SERVICE_TYPE_INVOICES = 'invoices';
    
    protected $base_url;
    protected $firewall;
    protected $client_id;
    protected $client_secret;
    
    protected $error_type;
    protected $error_description;

    /**
     * __construct: client constructor. 
     *
     * @param string $client_id OAuth2 server client id
     * @param string $client_secret OAuth2 server client secret
     * @param string $base_url server base url
     * @param string $mask service client implementation mask
     * @param string $firewall the firewall on the OAuth2 server (optional).
     * @param string $token_endpoint the token end point on the OAuth2 server (optional).
     * @param string $grant_type the client grant type (optional). Needs implementation if different from client credentials
     * @return a DSY Client.
     */
    public function __construct($client_id, $client_secret, $base_url, $firewall = '/api') {
        
        $this->base_url             = $base_url;
        $this->firewall             = $firewall;
        
        $this->client_id            = $client_id;
        $this->client_secret        = $client_secret;
    }
    
    public function setBaseUrl($base_url){
        $this->base_url     = $base_url;
    }
    
    public function setFireWall($firewall){
        $this->firewall     = $firewall;
    }
    
    public function getServiceUrl($mask){
        return $this->base_url . $this->firewall . $mask;
    }
    
    public function getError(){
        if($this->error_type && $this->error_description){
            return $this->error_type.': '.$this->error_description;
        }
        
        if($this->error_type){
            return $this->error_type;
        }
        
        return 'No error';
    }
    
    public function service($service){
        
        switch ($service) {
            case self::CLIENT_SERVICE_TYPE_INVOICES:{
                return InvoicesService::newService($this);
            }
            case self::CLIENT_SERVICE_TYPE_LOGS:{
                return LogsService::newService($this);
            }
            break;
        }
        
        throw new \Exception('Unkonw service');
    }
    
    public static function availableServices(){
        $services = array();
        $services[] = self::CLIENT_SERVICE_TYPE_INVOICES;
        $services[] = self::CLIENT_SERVICE_TYPE_LOGS;
        return $services;
    }
    
    abstract public function fetch($uri, $parameters = null);
}

