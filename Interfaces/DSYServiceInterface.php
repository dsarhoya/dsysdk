<?php

namespace DSYSDK\Interfaces;

use DSYSDK\Clients\DSYBaseClient;

/**
 * Description of DSYClientInterface
 *
 * @author matias
 */
interface DSYServiceInterface {
    
    /**
     * clientFactory: fast constructor for the client 
     *
     * @return a DSY Client.
     */
    public static function newService(DSYBaseClient $client);
}
