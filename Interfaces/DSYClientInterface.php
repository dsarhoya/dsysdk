<?php

namespace DSYSDK\Interfaces;

/**
 * Description of DSYClientInterface
 *
 * @author matias
 */
interface DSYClientInterface {
    
    /**
     * clientFactory: fast constructor for the client 
     *
     * @return a DSY Client.
     */
    public function fetch($uri, $parameters = null);
}
